import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day1.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

var data:Array<{number:number}> = [];
let last:number = -1;
let lastWindow = -1;
let numberOfIncreases = 0;
let numberOfWindowIncreases = 0;
let countstack:number[] = [];
reader.on("line", (l:string) => {
    let lineAsNumber:number = parseInt(l);
    if (last >= 0 && lineAsNumber > last) {
        numberOfIncreases++;
    }

    countstack.push(lineAsNumber);
    if (countstack.length > 3) {
        countstack.shift();
    } else if (countstack.length < 3) {
        return;
    }
    let sum:number = 0;
    countstack.forEach(element => {
        sum += element;
    });
    if (lastWindow == -1) {
        lastWindow = sum;
    } else {
        if (sum > lastWindow) {
            numberOfWindowIncreases++;
        }
        lastWindow = sum;
    }

    last = lineAsNumber;
})
reader.on("close", () => {
    console.log(`Number of increases: ${numberOfIncreases}`);
    console.log(`Number of window increases: ${numberOfWindowIncreases}`);
})


