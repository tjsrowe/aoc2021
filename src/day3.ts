import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day3.txt';
let counts:number[] = [];
let lineValues:number[] = [];
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))
let lines = 0;
let maxBits:number = 0;

reader.on("line", (l:string) => {
    if (counts.length == 0) {
        for (let i = 0;i < l.length;i++) {
            counts[i] = 0;
        }
    }

    let intForLine:number = 0;
    for (let i = 0;i < l.length;i++) {
        intForLine = intForLine << 1;

        if (l.charAt(i) == '1') {
            counts[i]++;
            intForLine = intForLine | 1;
        }
        if (i >= maxBits) {
            maxBits = i;
        }
    }
    lineValues[lines] = intForLine;
    lines++;
})

function findDefinedValue(arr:number[]):number|undefined {
    for (let i = 0;i < arr.length;i++) {
        if (arr[i] != undefined) {
            return arr[i];
        }
    }
    return undefined;
}

function isBitMostCommonlySet(arr:number[], bit:number):boolean {
    let setCount:number = 0;
    let notSetCount:number = 0;
    for (let i:number = 0;i < arr.length;i++) {
        if (arr[i] == undefined) {
            continue;
        }
        let check:number = arr[i] & bit;
        if (check == bit) {
            setCount++;
        } else {
            notSetCount++;
        }
    }
    return setCount >= notSetCount;
}

reader.on("close", () => {
    let min = 9999999;
    let max = 0;

    let minBits = lines / 2;
    let gammaRate:number = 0;
    let epsilonRate:number = 0;

    for (let i = 0;i < counts.length;i++) {
        gammaRate = gammaRate << 1;
        epsilonRate = epsilonRate << 1;

        if (counts[i] >= minBits) {
            gammaRate = gammaRate | 1;
        } else {
            epsilonRate = epsilonRate | 1;
        }
    }

    console.log(`Min: ${min} Max: ${max}`);
    console.log(`Gamma: ${gammaRate} Epsilon: ${epsilonRate}`);

    let powerConsumption:number = gammaRate * epsilonRate;
    console.log(`PowerConsumption: ${powerConsumption}`);

    let bitsToCheck:number = 1 << (maxBits);

    let remainingGammaValues:number[] = [];
    let remainingEpsilonValues:number[] = [];

    for (let i = 0;i < lineValues.length;i++) {
        remainingGammaValues[i] = lineValues[i];
        remainingEpsilonValues[i] = lineValues[i];
    }

    let remainingGammaCount:number = remainingGammaValues.length;
    let remainingEpsilonCount:number = remainingEpsilonValues.length;

    let finalOxygenValue:number|undefined = undefined;
    let finalCO2Value:number|undefined = undefined;

    for (let currentBit:number = bitsToCheck;currentBit > 0;currentBit = currentBit >> 1) {
        if (finalOxygenValue == undefined) {
            let isCommonInGamma:boolean = isBitMostCommonlySet(remainingGammaValues, currentBit);
            let gammaAtBit:number = 0;
            if (isCommonInGamma) {
                gammaAtBit = currentBit;
            }
    
            for (let i = 0;i < remainingGammaValues.length;i++) {
                if (remainingGammaValues[i] != undefined) {
                    let valueMatchedBit:number = remainingGammaValues[i] & currentBit;
                    if (gammaAtBit != valueMatchedBit) {
                        delete remainingGammaValues[i];
                        remainingGammaCount--;
                    }
                }
                if (remainingGammaCount == 1) {
                    finalOxygenValue = findDefinedValue(remainingGammaValues);
                    break;
                }
            }
        }

        if (finalCO2Value == undefined) {
            let isCommonInGamma:boolean = isBitMostCommonlySet(remainingEpsilonValues, currentBit);
            let epsilonAtBit:number = 0;
            if (!isCommonInGamma) {
                epsilonAtBit = currentBit;
            }

            for (let i = 0;i < remainingEpsilonValues.length;i++) {
                if (remainingEpsilonValues[i] != undefined) {
                    let valueMatchedBit:number = remainingEpsilonValues[i] & currentBit;
                    if (epsilonAtBit != valueMatchedBit) {
                        delete remainingEpsilonValues[i];
                        remainingEpsilonCount--;
                    }
                }
                if (remainingEpsilonCount == 1) {
                    finalCO2Value = findDefinedValue(remainingEpsilonValues);
                    break;
                }
            }
        }
    }
    console.log(`Final Oxygen: ${finalOxygenValue}   CO2: ${finalCO2Value}`);
    let lifeSupportRating:number = finalOxygenValue! * finalCO2Value!;
    console.log(`Final value: ${lifeSupportRating}`);
})


