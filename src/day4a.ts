import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day4.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let selectedNumbers:string[];
let boards:string[][] = [];
let currentBoard:number = 0;
let currentBoardData:string[] = [];

function processNumbers(line:string):void {
    selectedNumbers = line.trim().split(",");
}

function finaliseBoard():void {
    const arr:string[] = [];
    currentBoardData.forEach((num) => {
        arr.push(num);
    });
    boards[currentBoard] = arr;
}

function processBoardLine(l:string) {
    if (l.trim() == "") {
        if (currentBoardData.length > 0 && currentBoardData.length != 0) {
            finaliseBoard();
            currentBoard++;
        }
        currentBoardData = [];
    } else {
        let data:string[] = l.trim().split(" ");
        for (let i = 0;i < data.length;i++) {
            if (data[i].trim() != "") {
                currentBoardData.push(data[i].trim());
            }
        }
    }
}

reader.on("line", (l:string) => {
    lineNumber++;
    if (lineNumber == 1) {
        processNumbers(l);
    } else {
        processBoardLine(l);
    }
})

function bingo(originalState:string[], finalState:string[], finalNumber:string):number {
    let finalNumberInt:number = parseInt(finalNumber);
    let total:number = 0;
    finalState.forEach((number) => {
        if (number.trim() != "") {
            let currentNumberValue:number = parseInt(number);
            total += currentNumberValue;
        }
    });
    return total * finalNumberInt;
}

function removeFromBoard(board:string[], value:string):void {
    for (let i:number = 0;i < board.length;i++) {
        if (board[i] == value) {
            board[i] = "";
        }
    }
}

function checkCols(board:string[]):boolean {
    for (let i:number = 0;i < 5;i++) {
        if (checkCol(board, i)) {
            return true;
        }
    }
    return false;
}

function checkRows(board:string[]):boolean {
    for (let i:number = 0;i < 5;i++) {
        if (checkRow(board, i)) {
            return true;
        }
    }
    return false;
}

function checkCol(board:string[], col:number):boolean {
    let returnValue:boolean = true;
    for (let i = col;i < board.length;i += 5) {
        if (board[i] != "") {
            returnValue = false;
            break;
        }
    }
    return returnValue;
}

function checkRow(board:string[], rowNumber:number):boolean {
    let startingIndex = ((rowNumber) * 5);
    let returnValue:boolean = true;
    for (let i:number = startingIndex;i < startingIndex+5;i++) {
        if (board[i] != "") {
            returnValue = false;
            return false;
        }
    }
    return returnValue;
}

function checkBingoState(board:string[]):boolean {
    let rowsMatched:boolean = checkRows(board);
    let colsMatched:boolean = checkCols(board);

    if (rowsMatched || colsMatched) {
        return true;
    }
    return false;
}

reader.on("close", () => {
    let workingBoards:string[][] = [];
    for (let i = 0;i < boards.length;i++) {
        let board:string[] = boards[i];
        let boardCopy:string[] = board.concat([]);
        workingBoards[i] = boardCopy;
    }
    let finished:boolean = false;
    selectedNumbers.forEach((bingoNumber:string) => {
        if (!finished) {
            for (let i = 0;i < workingBoards.length;i++) {
                let board:string[] = workingBoards[i];
                removeFromBoard(board, bingoNumber);
                if (checkBingoState(board)) {
                    let winningBoardOriginalState:string[] = boards[i];
                    let result:number = bingo(winningBoardOriginalState, board, bingoNumber);
                    console.log(`Bingo! ${result} (last number was ${bingoNumber})`);
                    finished = true;
                    break;
                }
            }
        }
        if (finished == true) {
            return;
        }
    });
})


