import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day9.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let count:number = 0;
let caveMap:number[][] = [];
let caveY:number = -1;
let caveX:number = 0;

reader.on("line", (l:string) => {
    caveX++;

    let vals:number[] = [];
    for (let i = 0;i < l.length;i++) {
        let num:number = parseInt(l.charAt(i));
        vals.push(num);
    }
    caveMap.push(vals);
    if (caveY < 0) {
        caveY = l.length;
    }
    lineNumber++;
});

function isLowestPoint(x:number, y:number):boolean {
    let minx:number = Math.max(0, x-1);
    let miny:number = Math.max(0, y-1);
    let maxx:number = Math.min(caveX-1, x+1);
    let maxy:number = Math.min(caveY-1, y+1);

    let currentPositionHeight:number = caveMap[x][y];

    let isLowest:boolean = true;
    for (let i:number = minx;i <= maxx;i++) {
        for (let j:number = miny;j <= maxy;j++) {
            if (x == i && y == j) {
                continue;
            }
            let comparedHeight:number = caveMap[i][j];
            if (comparedHeight < currentPositionHeight) {
                return false;
            }
        }
    }

    return isLowest;
}

reader.on("close", () => {
    let lowPoints:number = 0;
    let lowPointSum:number = 0;
    for (let x:number = 0;x < caveX;x++) {
        for (let y:number = 0;y < caveY;y++) {
            let isLowest:boolean = isLowestPoint(x, y);
            if (isLowest) {
                let valueAtIndex = caveMap[x][y];
                lowPointSum += valueAtIndex + 1;
                lowPoints++;
            }
        }
    }
    console.log(`Number seen: ${lowPoints} with sum ${lowPointSum}`);
});


