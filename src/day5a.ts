import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day5.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let gridSize:number = 1000;

function initGrid():number[][] {
    let output:number[][] = [];
    for (let x:number = 0;x < gridSize;x++) {
        output[x] = [];
        for (let y = 0;y < gridSize;y++) {
            output[x][y] = 0;
        }
    }
    return output;
}

let lineGrid:number[][] = initGrid();

function countOverlaps():number {
    let overlaps:number = 0;
    for (let x = 0;x < gridSize;x++) {
        for (let y = 0;y < gridSize;y++) {
            if (lineGrid[y][x] > 1) {
                overlaps++;
            }
        }
    }
    return overlaps;
}

function addToGrid(start:string[], end:string[]):void {
    let x1:number = parseInt(start[0]);
    let y1:number = parseInt(start[1]);
    let x2:number = parseInt(end[0]);
    let y2:number = parseInt(end[1]);

    if (x2 < x1) {
        let tmp:number = x2;
        x2 = x1;
        x1 = tmp;
    }

    if (y2 < y1) {
        let tmp:number = y2;
        y2 = y1;
        y1 = tmp;
    }

    if (x1 == x2) {
        for (let y = y1;y <= y2;y++) {
            lineGrid[y][x1]++;
        }
    } else if (y1 == y2) {
        for (let x = x1;x <= x2;x++) {
            lineGrid[y1][x]++;
        }
    }
}

reader.on("line", (l:string) => {
    lineNumber++;
    let lines = l.split(" -> ");
    let start = lines[0].split(",");
    let end = lines[1].split(",");

    addToGrid(start, end);
})

reader.on("close", () => {
    let totalOverlaps:number = countOverlaps();
    console.log(`Total overlaps: ${totalOverlaps}`);
})


