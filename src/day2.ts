import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day2.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))


let hpos:number = 0;
let depth:number = 0;
let aim:number = 0;
let aimdepth:number = 0;
let aimhpos:number = 0;

reader.on("line", (l:string) => {
    let parts:string[] = l.split(" ");
//    parts.forEach(element => {
        let command = parts[0];
        let unit:number = parseInt(parts[1]);
        if ('forward' == command) {
            hpos += unit;
            aimdepth += (unit * aim);
        } else if ('down' == command) {
            aim += unit;
            depth += unit;
        } else if ('up' == command) {
            aim -= unit;
            depth -= unit;
        } else {
            console.log(`Command: ${command}`);
        }
//    });
})
reader.on("close", () => {
    console.log(`Depth: ${depth} Hpos: ${hpos}`);
    let multiplication:number = depth * hpos;
    let aimmul:number = aimdepth * hpos;
    console.log(`Multiplication: ${multiplication}.  Aim multiplication: ${aimmul}`);
})


