import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';
import { deflateSync } from 'zlib';

const DATA_FILE = 'day6.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

const DAYS:number = 80;

let lineNumber:number = 0;
let lanternfish:number[] = [];

reader.on("line", (l:string) => {
    let lfstr:string[] = l.split(",");
    lfstr.forEach((str) => {
        lanternfish.push(parseInt(str));
    });
    lineNumber++;
})

function recalculateLanternfish():number[] {
    let output:number[] = [];
    let newFish = 0;
    for (let i:number = 0;i < lanternfish.length;i++) {
        let fish:number = lanternfish[i];
        if (0 == fish) {
            output.push(6);
            newFish++;
        } else {
            output.push(fish-1);
        }
    }
    for (let n:number = 1;n <= newFish;n++) {
        output.push(8);
    }
    return output;
}

reader.on("close", () => {
    for (let d:number = 1;d <= DAYS;d++) {
        lanternfish = recalculateLanternfish();
    }
    let numberOfLanternfish = lanternfish.length;
    console.log(`Total Lanternfish: ${numberOfLanternfish}`);
})


