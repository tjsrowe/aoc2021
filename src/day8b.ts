import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day8.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let count:number = 0;

function containsAllElements(testValue:string, knownValue:string):boolean {
    for (let i:number = 0;i < knownValue.length;i++) {
        if (testValue.indexOf(knownValue.charAt(i)) == -1) {
            return false;
        }
    }
    return true;
}

function removeFromValue(inputValue:string, valuesToRemove:string):string {
    let workingString:string = inputValue;
    for (let i:number = 0;i < valuesToRemove.length;i++) {
        let ch:string = valuesToRemove.charAt(i);
        let index:number = workingString.indexOf(ch);
        if (index == 0) {
            workingString = workingString.substring(1);
        } else if (index == workingString.length-1) {
            workingString = workingString.substring(0, index);
        } else {
            workingString = workingString.substring(0, index) + workingString.substring(index+1);
        }
    }
    return workingString;
}

function sortString(input:string):string {
    let chrs:string[] = input.split('');
    chrs = chrs.sort();
    let finalMapping:string = "";
    for (let j:number = 0;j < chrs.length;j++) {
        finalMapping = finalMapping + chrs[j];
    }
    return finalMapping;
}

function sortMappings(mappings:string[]):void {
    for (let i:number = 0;i < mappings.length;i++) {
        let currentMapping:string = mappings[i];
        if (currentMapping == undefined) {
        } else {
            mappings[i] = sortString(currentMapping);
        }
    }
}

function getDisplayValueForOutput(mappings:string[], values:string[]) {
    let num:number = 0;
    let radix:number = 1;
    values.forEach((element:string) => {
        element = sortString(element);
        let singleNum:number = getDisplayValueForString(mappings, element);
        num *= radix;
        num += singleNum;
        radix = 10;
    });
    return num;
}

function getDisplayValueForString(mappings:string[], value:string):number {
    for (let i:number = 0;i < mappings.length;i++) {
        if (mappings[i] == value) {
            return i;
        }
    }
    return -1;
}

reader.on("line", (l:string) => {
    let data:string[];
    let outputs:string[];
    let rows:string[] = l.split("|");
    
    data = rows[0].trim().split(" ");
    outputs = rows[1].trim().split(" ");

    let mappings:string[] = [];
    let unknown:string[] = [];

    data.forEach((value) => {
        let trval:string = value.trim();
        let sorted:string = sortString(trval);
        let len:number = trval.length;
        if (len == 2) {
            mappings[1] = sorted;
        } else if (len == 4) {
            mappings[4] = sorted;
        } else if (len == 3) {
            mappings[7] = sorted;
        } else if (len == 7) {
            mappings[8] = sorted;
        } else {
            unknown.push(sorted);
        }
    });

    let searchSegments9or5 = removeFromValue(mappings[4], mappings[1]);

    unknown.forEach((value) => {
        let idx:number = -1;
        let len:number = value.length;
        if (len == 6 && !containsAllElements(value, mappings[1])) {
            idx = 6;
        } else if (len == 5 && containsAllElements(value, mappings[1])) {
            idx = 3;
        } else if (len == 6) {
            if (containsAllElements(value, searchSegments9or5)) {
                idx = 9;
            } else {
                idx = 0;
            }
        } else if (len == 5) {
            if (containsAllElements(value, searchSegments9or5)) {
                idx = 5;
            } else {
                idx = 2;
            }
        }
        mappings[idx] = value;
    });

    sortMappings(outputs);
    let value:number = getDisplayValueForOutput(mappings, outputs);
    count += value;

    lineNumber++;
});

reader.on("close", () => {
    console.log(`Number seen: ${count}`);
});


