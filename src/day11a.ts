import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day11.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let count:number = 0;
let MAX_STEPS = 100;

let octopusPowerLevels:number[][] = [];

reader.on("line", (l:string) => {
    let octopusLine:number[] = [];
    for (let i:number = 0;i < l.length;i++) {
        let ch:string = l.charAt(i);
        let val:number = parseInt(ch);
        octopusLine[i] = val;
    }
    octopusPowerLevels.push(octopusLine);
    lineNumber++;
});

function hasHighPoweredOctopus(flashedThisStep:boolean[][]):boolean {
    for (let x:number = 0;x < octopusPowerLevels.length;x++) {
        for (let y:number = 0;y < octopusPowerLevels[x].length;y++) {
            if (octopusPowerLevels[x][y] > 9 && !flashedThisStep[x][y]) {
                return true;
            }
        }
    }
    return false;
}

function addPowerToSurrounding(x:number, y:number) {
    let minx:number = Math.max(0, x-1);
    let miny:number = Math.max(0, y-1);
    let maxx:number = Math.min(octopusPowerLevels.length-1, x+1);
    let maxy:number = Math.min(octopusPowerLevels[0].length-1, y+1);

    for (let i:number = minx;i <= maxx;i++) {
        for (let j:number = miny;j <= maxy;j++) {
            if (i != x || j != y) {
                octopusPowerLevels[i][j]++;
            }
        }
    }
}

function resetFlashes(flashStepTrack:boolean[][]) {
    for (let x:number = 0;x < flashStepTrack.length;x++) {
        for (let y:number = 0;y < flashStepTrack[x].length;y++) {
            if (flashStepTrack[x][y] == true) {
                octopusPowerLevels[x][y] = 0;
            }

            flashStepTrack[x][y] = false;
        }
    }
}

function flashOctopuses(flashedThisStep:boolean[][]):number {
    let flashCount:number = 0;
    for (let x:number = 0;x < octopusPowerLevels.length;x++) {
        for (let y:number = 0;y < octopusPowerLevels[x].length;y++) {
            if (!flashedThisStep[x][y] && octopusPowerLevels[x][y] > 9) {
                addPowerToSurrounding(x, y);
                flashedThisStep[x][y] = true;
                flashCount++;
            }
        }
    }
    return flashCount;
}

function increaseAllLevelsByOne():void {
    for (let x:number = 0;x < octopusPowerLevels.length;x++) {
        for (let y:number = 0;y < octopusPowerLevels[x].length;y++) {
            octopusPowerLevels[x][y]++;
        }
    }
}

reader.on("close", () => {
    let flashedThisStep:boolean[][] = [];
    for (let i:number = 0;i < octopusPowerLevels.length;i++) {
        flashedThisStep[i] = [];
        for (let j:number = 0;j < octopusPowerLevels[0].length;j++) {
            flashedThisStep[i][j] = false;
        }
    }

    let totalFlashes:number = 0;
    for (let step:number = 1;step <= MAX_STEPS;step++) {
        let shouldContinue:boolean = true;
        increaseAllLevelsByOne();

        while (shouldContinue) {
            let flashCount = flashOctopuses(flashedThisStep);
            totalFlashes += flashCount;

            shouldContinue = hasHighPoweredOctopus(flashedThisStep);
        }
        resetFlashes(flashedThisStep);
    }

    console.log(`Total flashes: ${totalFlashes}`);
});


