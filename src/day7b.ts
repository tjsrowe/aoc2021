import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day7.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let positions:number[] = [];
let maxPosition:number = -1;

reader.on("line", (l:string) => {
    let positionStr:string[] = l.split(",");
    let positionInt:number[] = [];
    positionStr.forEach((p) => {
        let i:number = parseInt(p);
        positionInt.push(i);
        if (i > maxPosition) {
            maxPosition = i;
        }
    });
    
    positions = positionInt;

    lineNumber++;
});

reader.on("close", () => {
    let minCost = 99999999;
    for (let i:number = 0;i < maxPosition;i++) {
        let positionCost:number = 0;
        positions.forEach((p) => {
            let movePositions:number = i-p;
            if (p > i) {
                movePositions = p-i;
            }

            let cost:number = 0;
            for (let i = 1;i <= movePositions;i++) {
                cost += i;
            }
            
            positionCost += cost;
        });
        if (positionCost < minCost) {
            minCost = positionCost;
        }
    }

    console.log(`Min cost: ${minCost}`);
});


