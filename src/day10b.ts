import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day10.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let scores:number[] = [];

reader.on("line", (l:string) => {
    let stack:string[] = [];
    let valid:boolean = true;

    let len:number = l.length;
    for (let i = 0;i < len;i++) {
        let ch:string = l.charAt(i);
        switch (ch) {
            case '(':
            case '[':
            case '{':
            case '<':
                stack.push(ch);
                continue;
        }
        let top:string|undefined = stack.pop();

        switch (ch) {
            case ')':
                if (top != '(') {
                    valid = false;
                }
                break;
            case ']':
                if (top != '[') {
                    valid = false;
                }
                break;
            case '}':
                if (top != '{') {
                    valid = false;
                }
                break;
            case '>':
                if (top != '<') {
                    valid = false;
                }
                break;
        }
        if (!valid) {
            break;
        }
    }

    if (valid) {
        console.log(`Remaining stack is ${stack}`);
        let totalPoints = 0;
        while (stack.length > 0) {
            let top:string|undefined = stack.pop();
            let points = 0;
            switch (top) {
                case '(':
                    points = 1;
                    break;
                case '[':
                    points = 2;
                    break;
                case '{':
                    points = 3;
                    break;
                case '<':
                    points = 4;
                    break;
            }
            totalPoints *= 5;
            totalPoints += points;
        }
        scores.push(totalPoints);
    }

    lineNumber++;
});

reader.on("close", () => {
    let sorted:number[] = scores.sort((a, b) => { return a - b });
    let midPosition:number = Math.trunc(sorted.length / 2);
    let midValue:number = sorted[midPosition];

    console.log(`Middle value: ${midValue}`);
});
