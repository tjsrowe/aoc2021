import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day10.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let score:number = 0;

reader.on("line", (l:string) => {
    let stack:string[] = [];

    let len:number = l.length;
    for (let i = 0;i < len;i++) {
        let ch:string = l.charAt(i);
        switch (ch) {
            case '(':
            case '[':
            case '{':
            case '<':
                stack.push(ch);
                continue;
        }
        let top:string|undefined = stack.pop();
        let valid:boolean = true;
        let points:number = 0;

        switch (ch) {
            case ')':
                if (top != '(') {
                    valid = false;
                    points = 3;
                }
                break;
            case ']':
                if (top != '[') {
                    valid = false;
                    points = 57;
                }
                break;
            case '}':
                if (top != '{') {
                    valid = false;
                    points = 1197;
                }
                break;
            case '>':
                if (top != '<') {
                    valid = false;
                    points = 25137;
                }
                break;
        }
        if (!valid) {
            score += points;
            break;
        }
    }

    lineNumber++;
});

reader.on("close", () => {
    console.log(`Number seen: ${score}`);
});
