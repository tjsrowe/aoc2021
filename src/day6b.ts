import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';
import { deflateSync } from 'zlib';

const DATA_FILE = 'day6.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

const DAYS:number = 256;

let lineNumber:number = 0;
let lanternfish:number[] = [];
let lanternfishDayCounts:number[] = [];

reader.on("line", (l:string) => {
    let lfstr:string[] = l.split(",");
    lfstr.forEach((str) => {
        lanternfish.push(parseInt(str));
    });
    lineNumber++;
})

function recalculateLanternfish():number[] {
    let newData:number[] = [];
    let newSixes = lanternfishDayCounts[0];
    for (let i = 1;i < lanternfishDayCounts.length;i++) {
        newData[i-1] = lanternfishDayCounts[i];
    }
    newData[6] += newSixes;
    newData[8] = newSixes;

    return newData;
}

function convertLanternfishToCount() {
    for (let i = 0;i <= 8;i++) {
        lanternfishDayCounts[i] = 0;
    }

    for (let i = 0;i < lanternfish.length;i++) {
        let fish:number = lanternfish[i];
        lanternfishDayCounts[fish]++;
    }
}

function countLanternfish() {
    let count = 0;
    for (let i = 0;i < lanternfishDayCounts.length;i++) {
        count += lanternfishDayCounts[i];
    }
    return count;
}

reader.on("close", () => {
    convertLanternfishToCount();
    for (let d:number = 1;d <= DAYS;d++) {
        lanternfishDayCounts = recalculateLanternfish();
    }
    let numberOfLanternfish = countLanternfish();
    console.log(`Total Lanternfish: ${numberOfLanternfish}`);
})


