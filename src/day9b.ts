import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day9.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let caveMap:number[][] = [];
let caveY:number = -1;
let caveX:number = 0;

reader.on("line", (l:string) => {
    caveX++;

    let vals:number[] = [];
    for (let i = 0;i < l.length;i++) {
        let num:number = parseInt(l.charAt(i));
        vals.push(num);
    }
    caveMap.push(vals);
    if (caveY < 0) {
        caveY = l.length;
    }
    lineNumber++;
});

function basinContains(basinCoordinates:number[][], coord:number[]):boolean {
    let x:number = coord[0];
    let y:number = coord[1];
    let result:boolean = false;
    basinCoordinates.forEach((basinCoord) => {
        if (basinCoord[0] == x && basinCoord[1] == y) {
            result = true;
            return;
        }
    });
    return result;
}

function expandBasin(basinCoordinates:number[][], newCoordinates:number[][]):number[][] {
    let newlyAdded:number[][] = [];

    newCoordinates.forEach((coord) => {
        let x:number = coord[0];
        let y:number = coord[1];
        let minx:number = Math.max(0, x-1);
        let miny:number = Math.max(0, y-1);
        let maxx:number = Math.min(caveX-1, x+1);
        let maxy:number = Math.min(caveY-1, y+1);
    
        if (!basinContains(basinCoordinates, coord)) {
            basinCoordinates.push(coord);
        }

        for (let i:number = minx;i <= maxx;i++) {
            for (let j:number = miny;j <= maxy;j++) {
                if (x == i && y == j) {
                    continue;
                }
                if ((i == x-1 && j == y-1) ||
                    (i == x-1 && j == y+1) ||
                    (i == x+1 && j == y-1) ||
                    (i == x+1 && j == y+1)) {
                        continue;
                }

                let comparedHeight:number = caveMap[i][j];
                if (comparedHeight == 9) {
                    continue;
                }

                let checkCoord = [i, j];
                if (basinContains(basinCoordinates, checkCoord)) {
                    continue;
                }
                    
                newlyAdded.push(checkCoord);
                basinCoordinates.push(checkCoord);
            }
        }
    });
    return newlyAdded;
}

function removeCoordinatesFromMap(points:number[][]):void {
    points.forEach((point) => {
        caveMap[point[0]][point[1]] = -1;
    });
}

reader.on("close", () => {
    let basins:number[] = [];

    for (let x:number = 0;x < caveX;x++) {
        for (let y:number = 0;y < caveY;y++) {
            if (caveMap[x][y] == -1 || caveMap[x][y] == 9) {
                continue;
            }

            let point:number[] = [x,y];
            let basinCoordinates:number[][] = [];
    
            let newCoordinates:number[][] = [point];
            while(newCoordinates.length > 0) {
                newCoordinates = expandBasin(basinCoordinates, newCoordinates);
            }
            let basinSize = basinCoordinates.length;
            basins.push(basinSize);
            removeCoordinatesFromMap(basinCoordinates);
        }
    }
    basins.sort((n1,n2) => n2 - n1);
    let multiplied = basins[0] * basins[1] * basins[2];
    console.log(`Multiplied value: ${multiplied}`);
});


