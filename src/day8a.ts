import * as fs from 'fs';
import * as path from 'path';
import * as rd from 'readline';

const DATA_FILE = 'day8.txt';
var reader = rd.createInterface(fs.createReadStream(path.join(__dirname, `../data/${DATA_FILE}`)))

let lineNumber:number = 0;
let count:number = 0;

reader.on("line", (l:string) => {
    let data:string[];
    let outputs:string[];
    let rows:string[] = l.split("|");
    
    data = rows[0].trim().split(" ");
    outputs = rows[1].trim().split(" ");

    outputs.forEach((value) => {
        let len:number = value.trim().length;
        if (len == 2 || len == 3 || len == 4 || len == 7) {
            count++;
        }
    });

    lineNumber++;
});

reader.on("close", () => {
    console.log(`Number seen: ${count}`);
});


